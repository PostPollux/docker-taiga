
You can create you own custome theme like this:

1. copy one of the theme folders from "taiga-front/app/themes/" to this location.
2. rename the folder to the name you would like your theme to have
3. edit the two sass files
4. Build the taiga container and make sure you use the compiled frontend of the first stage
