#!/bin/bash

# Sleep when asked to, to allow the database time to start
# before Taiga tries to run /checkdb.py below.
: ${TAIGA_SLEEP:=0}
sleep $TAIGA_SLEEP

# Setup database automatically if needed
if [ -z "$TAIGA_SKIP_DB_CHECK" ]; then
  echo "Running database check"
  python -u /checkdb.py
  DB_CHECK_STATUS=$?

  # If database did not come online, exit the container with error
  if [ $DB_CHECK_STATUS -eq 2 ]; then
    echo "Failed to connect to database server or database does not exist."
    exit 1
  fi

  # Database migration check should be done in all startup in case of backend upgrade
  echo "Check for database migration"
  python manage.py migrate --noinput

  # Run the initial data seeder functions if we're starting with a fresh database
  if [ $DB_CHECK_STATUS -eq 1 ]; then
    echo "Configuring initial database"
    python manage.py loaddata initial_user # name: admin pw: 123123
    python manage.py loaddata initial_project_templates
  fi
fi


# Copy the config file fresh so that it works properly even if container is
# kept around for another run
rm -rf /taiga/conf.json;
cp /taiga/conf-orig.json /taiga/conf.json || exit 1

# In case of frontend upgrade, locales and statics should be regenerated
python manage.py compilemessages
python manage.py collectstatic --noinput


# convert bool values of several variables to lowercase
DEBUG="$(echo $DEBUG | sed -e 's/\(.*\)/\L\1/')"
PUBLIC_REGISTER_ENABLED="$(echo $PUBLIC_REGISTER_ENABLED | sed -e 's/\(.*\)/\L\1/')"
TAIGA_SSL_BY_REVERSE_PROXY="$(echo $TAIGA_SSL_BY_REVERSE_PROXY | sed -e 's/\(.*\)/\L\1/')"
TAIGA_SSL="$(echo $TAIGA_SSL | sed -e 's/\(.*\)/\L\1/')"
TAIGA_EVENTS_ENABLE="$(echo $TAIGA_EVENTS_ENABLE | sed -e 's/\(.*\)/\L\1/')"
USE_GRAVATAR="$(echo $USE_GRAVATAR | sed -e 's/\(.*\)/\L\1/')"


# Automatically replace placeholders in /taiga/conf.json with the values of the environment variables. Make sure those environment variables has been set, otherwise the resulting conf.json will be erroneous and Taiga will not work
sed -i "s/TAIGA_HOSTNAME/$TAIGA_HOSTNAME/g" /taiga/conf.json
sed -i "s/DEBUG_STATE/$DEBUG/g" /taiga/conf.json
sed -i "s/ALLOW_PUBLIC_REGISTRATION/$PUBLIC_REGISTER_ENABLED/g" /taiga/conf.json
sed -i "s/USE_GRAVATAR/$USE_GRAVATAR/g" /taiga/conf.json

# set maximum upload file size
sed -i "s/MAX_UPLOAD_SIZE/$MAX_UPLOAD_FILESIZE/g" /etc/nginx/ssl.conf
sed -i "s/MAX_UPLOAD_SIZE/$MAX_UPLOAD_FILESIZE/g" /etc/nginx/taiga.conf
sed -i "s/MAX_UPLOAD_SIZE/$MAX_UPLOAD_FILESIZE/g" /etc/nginx/taiga-events.conf

# Add all available themes to the conf.json
THEMESPATH=/usr/src/taiga-front-dist/dist/*/styles/*
THEMES=''
for f in $THEMESPATH
do
  # cut of the path
  themename="$(basename $f)"
  # get rid of "theme-" and ".css"
  themename="$(echo $themename | sed -e 's/theme-//g' | sed -e 's/.css//g')"
  # append the themename to the THEMES string  
  THEMES="$THEMES \"$themename\", "
done
# cut of last two characters to get rid of ", "
THEMES=${THEMES::-2}
# finally add the string to conf.json 
sed -i "s/THEMES/$THEMES/g" /taiga/conf.json


# Add correct events url to /taiga/conf.json if taiga events are enabled
if [ "$TAIGA_EVENTS_ENABLE" = "true" ]; then
  echo "Enabling Taiga Events"
  sed -i "s/eventsUrl\": null/eventsUrl\": \"ws:\/\/$TAIGA_HOSTNAME\/events\"/g" /taiga/conf.json

  # make sure that nginx works properly as a proxy to redirect the events url
  sed -i "s/EVENT_SERVER_PORT/$EVENT_SERVER_PORT/g" /etc/nginx/taiga-events.conf
  mv /etc/nginx/taiga-events.conf /etc/nginx/conf.d/default.conf
fi


# Handle enabling/disabling SSL
if [ "$TAIGA_SSL_BY_REVERSE_PROXY" = "true" ]; then
  echo "Enabling external SSL support! SSL handling must be done by a reverse proxy or a similar system"
  sed -i "s/http:\/\//https:\/\//g" /taiga/conf.json
  sed -i "s/ws:\/\//wss:\/\//g" /taiga/conf.json
elif [ "$TAIGA_SSL" = "true" ]; then
  echo "Enabling SSL support!"
  sed -i "s/http:\/\//https:\/\//g" /taiga/conf.json
  sed -i "s/ws:\/\//wss:\/\//g" /taiga/conf.json
  mv /etc/nginx/ssl.conf /etc/nginx/conf.d/default.conf
elif grep -q "wss://" "/taiga/conf.json"; then
  echo "Disabling SSL support!"
  sed -i "s/https:\/\//http:\/\//g" /taiga/conf.json
  sed -i "s/wss:\/\//ws:\/\//g" /taiga/conf.json
fi

# Start nginx service (need to start it as background process)
service nginx start

# Continue with the CMD command of the dockerfile.
exec "$@"