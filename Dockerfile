############################
# Stage one - build frontend
############################

# Start with NodeJS 7 on Debian "Stretch". Don't use Nodejs 8 as some dependencys of taiga frontend only work with Nodejs 7
FROM node:7-stretch as frontendbuild


##### no longer needed, as ruby sass is no longer used in taiga 
######################################################################

### Update System + install ruby
# set -x: make shell print commands while executing them
# rm -rf /var/lib/apt/lists/*: Clear Caches. Always do this after installing something with the package manager.
#RUN set -x && \
#    apt-get update && \
#    apt-get install -yq --no-install-recommends \
#        ruby \
#        ruby-all-dev \
#    && rm -rf /var/lib/apt/lists/*

### Install sass for ruby 
# "gem" is the package manager of ruby
# Executables of gems are stored in "USERNAME/.gem/ruby/VERSION/bin" and thus are not found by default by sh or bash.
#RUN gem install --user-install sass scss_lint

### Add the path to the gem executables to the $PATH variable
# the $PATH variable holds all the various paths where executables are supposed to be
# We append (>>) a new line with an updated PATH variable to the .bashrc file in the home directory.
# We now need to first restart bash on a fresh RUN command via ". ~/.bashrc" if we want to have access to the updated $PATH variable
#RUN echo PATH="$(ruby -r rubygems -e 'puts Gem.user_dir')/bin:$PATH" >> ~/.bashrc

########################################################################

WORKDIR /usr/src/taiga-front

# globally install gulp
RUN npm install -g gulp

# copy package.json file which contains all the dependencies that have to be installed
COPY taiga-front/package.json /usr/src/taiga-front/package.json

# install all dependencies found in package.json
RUN npm install

# now copy the whole frontend source code into the container. If we now change something we don't have to install the dependencies again.
COPY taiga-front/ /usr/src/taiga-front

# copy custom themes into the container so they can be compiled
COPY themes/ /usr/src/taiga-front/app/themes/

# finally build the frontend with "gulp" which automatically executes gulpfile.js. Use "deploy" as task which is defined in this file, because default would start a task "watch" that never ends...
RUN gulp deploy



###################################
# Stage two - final taiga container
###################################

### Start with Python 3.5 on Debian "Stretch"
FROM python:3.5-stretch

MAINTAINER Johannes Wuensch <j.wuensch@protonmail.com>

### Update System + install locales, gettext and Nginx
# set -x: make shell print commands while executing them
# gettext: is a package/framework that helps other software to produce multi-lingual messages. Taiga seems to use this.
# ca-certificates: installs a list of certificat authorities (CA's)so ssl applications can check the ssl connection
# rm -rf /var/lib/apt/lists/*: Clear Caches. Always do this after installing something with the package manager.
RUN set -x && \
    apt-get update && \
    apt-get install -yq --no-install-recommends \
        gettext \
        ca-certificates \
        nginx \
    && rm -rf /var/lib/apt/lists/*

# first copy only the requirements.txt and install the dependencies. If we had copied the whole source code and then installed the dependencies, they would need to be installed again each time we build the image if we have changed something in the source code.
WORKDIR /usr/src/taiga-back
COPY taiga-back/requirements.txt /usr/src/taiga-back/
RUN pip install --no-cache-dir -r requirements.txt

# now copy the source code of taiga backend into the container
COPY taiga-back /usr/src/taiga-back

# copy the compiled version of taiga frontend into the container. Use either the self compiled version from "frontendbuild" stage or directly the one of the "taiga-front-dist" git submodule
COPY --from=frontendbuild /usr/src/taiga-front/dist /usr/src/taiga-front-dist/dist
#COPY taiga-front-dist/ /usr/src/taiga-front-dist

# copy nginx settings
COPY conf/nginx/nginx.conf /etc/nginx/nginx.conf
COPY conf/nginx/taiga.conf /etc/nginx/conf.d/default.conf
COPY conf/nginx/ssl.conf /etc/nginx/ssl.conf
COPY conf/nginx/taiga-events.conf /etc/nginx/taiga-events.conf

# copy taiga configuration file into the container. Some variables in this file will be set by the entrypoint.sh script on startup.
RUN mkdir -p /taiga
COPY conf/taiga/conf.json /taiga/conf-orig.json

# copy local.py. It is supposed to hold custom taiga backend settings, but in this case it does nothing but importing the content of backend-settings.py which holds that information instead
COPY conf/taiga/local.py /taiga/local.py

# copy backend-settings.py into the container and rename it to docker.py which gets imported from local.py
COPY backend-settings.py /usr/src/taiga-back/settings/docker.py

# Setup symbolic links for configuration files
RUN ln -s /taiga/local.py /usr/src/taiga-back/settings/local.py && \
    ln -s /taiga/conf.json /usr/src/taiga-front-dist/dist/conf.json

# set some environment variables
ENV TAIGA_SSL=False \
    TAIGA_ENABLE_EMAIL=False \
    TAIGA_HOSTNAME=localhost \
    TAIGA_SECRET_KEY="!!!REPLACE-ME-j1475u1J^U*(y2fgd98u51u5981urf98u2o5uvoiiuzhlit3)!!!" \
    DEBUG=False \
    PUBLIC_REGISTER_ENABLED=False \
    USE_GRAVATAR=True \
    MAX_UPLOAD_FILESIZE=50M

# Copy the static files from all Django Applications in a folder. That's a specific Django command. It creates a folder called staticfiles at the root level. That helps, because you can point your frontend web server (e.g. nginx) to that single folder STATIC_ROOT and serve static files from a single location, rather than configure your web server to serve static files from multiple paths.
RUN python manage.py collectstatic --noinput

# copy a custom healthcheck shell script into the container
COPY docker-healthcheck.sh /usr/local/bin/

# Copy checkdb.py and docker-entrypoint.sh. Those will make sure that Taiga will be configured and run correctly when starting the container
COPY checkdb.py /checkdb.py
COPY docker-entrypoint.sh /docker-entrypoint.sh

# Define healthcheck with a custom shell script (we don't need a path for the healthcheck file as we copied it to the /usr/local/bin/ directory before)
HEALTHCHECK --interval=30s --timeout=3s --retries=3 CMD ["docker-healthcheck.sh"]

### Separate healthchecks for frontend and backend. As you can have only one healtcheck in a container we use the shell script healthcheck.sh instead to test the frontend and backend at the same time. Just for reference here is how you would do each of them by it's own, by checking if nginx static file serving is working. First is the frontend, second the backend.
# possible exit-codes: 0: ok/healthy, 1: error/unhealthy 
# command1 || command2: command2 is executed if and only if command1 has failed
# HEALTHCHECK --interval=30s --timeout=3s --retries=3 CMD curl --fail http://localhost:80/conf.json || exit 1
# HEALTHCHECK --interval=30s --timeout=3s --retries=3 CMD curl --fail http://localhost:80/api/v1/ || exit 1

# Handle logging: forward request and error logs of nginx to docker log collector by linking them to stdout and stderr 
RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

# Add a volume to make sure all uploaded media is saved in a volume and doesn't get lost when the container gets deleted.
VOLUME /usr/src/taiga-back/media

# expose http port (80) and https port (443) to the docker network 
EXPOSE 80 443

# Run entrypoint script that ckecks the database via checkdb.py, handles updates, replaces Variables in Nginx confs...
ENTRYPOINT ["/docker-entrypoint.sh"]

# finally run Taiga
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]