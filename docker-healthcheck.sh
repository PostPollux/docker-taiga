#!/bin/bash

# check both, frontend and backend 
if curl --fail http://localhost:80/conf.json && curl --fail http://localhost:80/api/v1/
 then
 	echo "Healthcheck succeeded! Both frontend and backend is reachable."
	exit 0
fi

echo "Healthcheck failed! Either the frontend or the backend was not reachable."
exit 1