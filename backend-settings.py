# Importing common provides default settings, see:
# https://github.com/taigaio/taiga-back/blob/master/settings/common.py
from .common import *


# Fetches an environment variable, and treats as a boolean.
# default state = False
#
# input: name of env var
# output: bool
def getenv_bool(envname: str) -> str:
    return envname in os.environ and os.getenv(envname).lower() == 'true'

#########################################
## PRIMARY DATABASE
#########################################

# RDS_* are the environment variables automatically created by
# elastic beanstalk. Specifying these here as backups allow the application
# to be more easily deployed there. They are only ever checked if the TAIGA_DB_*
# env vars don't exist.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('TAIGA_DB_NAME') or os.getenv('RDS_HOSTNAME'),
        'HOST': os.getenv('TAIGA_DB_HOST') or os.getenv('RDS_DB_NAME'),
        'USER': os.getenv('TAIGA_DB_USER') or os.getenv('RDS_USERNAME'),
        'PASSWORD': os.getenv('TAIGA_DB_PASSWORD') or os.getenv('RDS_PASSWORD'),
        'PORT': os.getenv('TAIGA_DB_PORT') or os.getenv('RDS_PORT') or '5432'
    }
}


#########################################
## HOSTNAME CONFIG
#########################################

TAIGA_HOSTNAME = os.getenv('TAIGA_HOSTNAME')

if getenv_bool("TAIGA_SSL") or getenv_bool("TAIGA_SSL_BY_REVERSE_PROXY"):
    PROTOCOL = 'https'
else:
    PROTOCOL = 'http'

SITES['api']['domain'] = TAIGA_HOSTNAME
SITES['front']['domain'] = TAIGA_HOSTNAME

SITES['api']['scheme'] = PROTOCOL
SITES['front']['scheme'] = PROTOCOL

MEDIA_URL  = PROTOCOL + '://' + TAIGA_HOSTNAME + '/media/'
STATIC_URL = PROTOCOL + '://' + TAIGA_HOSTNAME + '/static/'

SECRET_KEY = os.getenv('TAIGA_SECRET_KEY')



#########################################
## EVENTS/WEBSOCKETS
#########################################

if getenv_bool('TAIGA_EVENTS_ENABLE'):
    from .celery import *

    RABBIT_BROKER_URL = (
        'amqp://' +
        os.getenv('EVENT_USERNAME') + ':' + os.getenv('EVENT_PASSWORD') + '@' +
        os.getenv('EVENT_HOST') + ':' + os.getenv('EVENT_RABBIT_PORT') + '/' +
        os.getenv('EVENT_VIRTUAL_HOST')
    )

    EVENTS_PUSH_BACKEND = "taiga.events.backends.rabbitmq.EventsPushBackend"
    EVENTS_PUSH_BACKEND_OPTIONS = {"url": RABBIT_BROKER_URL}




#########################################
## EMAIL
#########################################

if getenv_bool('TAIGA_ENABLE_EMAIL'):


    DEFAULT_FROM_EMAIL = os.getenv('TAIGA_EMAIL_FROM')
    SERVER_EMAIL = os.getenv('TAIGA_EMAIL_FROM')

    # In seconds
    CHANGE_NOTIFICATIONS_MIN_INTERVAL = int(os.getenv('TAIGA_EMAIL_NOTIFICATIONS_INTERVAL') or 0)

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    
    EMAIL_USE_TLS = getenv_bool('TAIGA_EMAIL_USE_TLS')
    EMAIL_HOST = os.getenv('TAIGA_EMAIL_HOST')
    EMAIL_PORT = int(os.getenv('TAIGA_EMAIL_PORT'))
    EMAIL_HOST_USER = os.getenv('TAIGA_EMAIL_USERNAME')
    EMAIL_HOST_PASSWORD = os.getenv('TAIGA_EMAIL_PASSWORD')


    # GMAIL SETTINGS EXAMPLE

    #DEFAULT_FROM_EMAIL = "john@doe.com"
    #CHANGE_NOTIFICATIONS_MIN_INTERVAL = 300 #seconds

    #EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    #EMAIL_USE_TLS = True
    #EMAIL_HOST = 'smtp.gmail.com'
    #EMAIL_PORT = 587
    #EMAIL_HOST_USER = 'youremail@gmail.com'
    #EMAIL_HOST_PASSWORD = 'yourpassword'



#########################################
## REGISTRATION
#########################################

# Allow registration
PUBLIC_REGISTER_ENABLED = getenv_bool('PUBLIC_REGISTER_ENABLED')

# LIMIT ALLOWED DOMAINS FOR REGISTER AND INVITE
# None or [] values in USER_EMAIL_ALLOWED_DOMAINS means allow any domain
if 'ALLOWED_REGISTER_DOMAINS' in os.environ and len(os.getenv('ALLOWED_REGISTER_DOMAINS')) > 1:
    USER_EMAIL_ALLOWED_DOMAINS = os.getenv('ALLOWED_REGISTER_DOMAINS').split(",")
else:
    USER_EMAIL_ALLOWED_DOMAINS = None


#########################################
## FEEDBACK
#########################################

# Note: See config in taiga-front too
FEEDBACK_ENABLED = False
# FEEDBACK_EMAIL = "support@taiga.io"



DEBUG = getenv_bool('DEBUG')

# TEMPLATE_DEBUG = os.getenv('TEMPLATE_DEBUG'].lower() == 'true'